#!/usr/bin/perl 
#===============================================================================
#
#         FILE: issues2cl.pl
#
#        USAGE: ./issues2cl.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 09/06/13 17:13:09
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use App::Daemon qw ( daemonize );
use LWP::UserAgent;

use C4::Context;
use POSIX qw/strftime/;

daemonize();

# load a file of isbn into a hashref, keyed by biblionumber
my $covers;
open( COVERS, "<", "covers.txt" ) or die $!;
while ( my $line = <COVERS> ) {
    chomp($line);
    my @values = split( /\t/, $line );
    $covers->{ $values[0] } = $values[1];
}

my $time = strftime( '%Y-%m-%d %H:%M:%S', localtime );
while (1) {
    sleep 20;
    $time = get_stats( $time, $covers );
}

sub get_stats {
    my $time   = shift;
    my $covers = shift;
    open( my $filehandle, '>>', './output.txt' )
      or die "cannot open >> output.txt: $!";
    my $timenew = strftime( '%Y-%m-%d %H:%M:%S', localtime );
    my $select =
      "SELECT unix_timestamp(datetime) AS ts,branchname,type,biblio.title AS ti,
biblio.biblionumber AS bibnum
FROM statistics LEFT JOIN branches ON statistics.branch = branches.branchcode 
LEFT JOIN items ON statistics.itemnumber = items.itemnumber 
LEFT JOIN biblio ON items.biblionumber = biblio.biblionumber 
WHERE type IN ('issue','renew','return') AND branchname IS NOT NULL 
AND datetime >= ?
AND biblio.title IS NOT NULL ORDER BY datetime";

    my $sth = C4::Context->dbh->prepare($select);
    $sth->execute($time);

    while ( my $data = $sth->fetchrow_hashref() ) {
        $covers = check_covers( $data->{'bibnum'}, $covers, $data->{'ti'} );
        print $filehandle $data->{'ts'} . "|" . $data->{'ti'} . "|";
        if ( $data->{type} eq 'issue' ) {
            print $filehandle 'A';
        }
        elsif ( $data->{type} eq 'renew' ) {
            print $filehandle 'M';
        }
        else {
            print $filehandle 'D';
        }
        print $filehandle "|" . $data->{branchname} . "\n";
    }
    close $filehandle;
    return $timenew;
}

sub check_covers {
    my $biblionumber = shift;
    my $covers       = shift;
    my $title = shift;
    warn "biblionumber is $biblionumber";
    if ( exists $covers->{$biblionumber} ) {
        return $covers;
    }
    else {
        my $sth = C4::Context->dbh->prepare(
            "SELECT isbn FROM biblioitems WHERE biblionumber=?");
        $sth->execute($biblionumber);
        if ( my $data = $sth->fetchrow_hashref() ) {
            $covers->{$biblionumber} = $data->{'isbn'};
            open( COVERSOUT, '>>', 'covers.txt' );
#            print COVERSOUT $biblionumber . "\t" . $data->{'isbn'} . "\n";
            close COVERSOUT;
            get_cover($data->{'isbn'},$title);
        }
        else {
            return $covers;
        }
    }
}

sub get_cover {
    my $isbn      = shift;
    my $title     = shift;
    my $outputdir = 'covers';
    my $url       = "http://covers.openlibrary.org/b/isbn/" . $isbn . "-S.jpg";
    my $ua        = LWP::UserAgent->new();
    $ua->agent('Koha_Library_System');
    my $image_file = $outputdir . '/' . $title . '.jpg';
    my $response = $ua->get( $url, ':content_file' => $image_file );
    unless ( $response->is_success && $response->content_length > 400 ) {
        unlink($image_file);
    }
    return;
}

